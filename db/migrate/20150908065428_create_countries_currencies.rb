class CreateCountriesCurrencies < ActiveRecord::Migration
  def change
    create_table :countries_currencies, :id => false do |t|
      t.references :country, :currency
    end

    add_index :countries_currencies, [:country_id, :currency_id],
      name: "countries_currencies_index",
      unique: true
  end
end
