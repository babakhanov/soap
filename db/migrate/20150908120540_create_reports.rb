class CreateReports < ActiveRecord::Migration
  def change
    create_table :reports do |t|
      t.string :runned_by
      t.string :info

      t.timestamps null: false
    end
  end
end
