class CountriesController < ApplicationController

  respond_to :json
    
  def index
    @countries = Country.order(name: :asc)
    respond_with @countries
  end

  def show
    @country = Country.find(params[:id])
    respond_with @country
  end
end
