class ReportsController < ApplicationController

  respond_to :json

  def index
    @reports =  Report.order(created_at: :desc)
    respond_with @reports
  end
end
