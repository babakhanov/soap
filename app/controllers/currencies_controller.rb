class CurrenciesController < ApplicationController

  respond_to :json
  
  def index
    if Currency.all.count == 0
      get_currencies
    end
    if Report.where("created_at > :time", time: 5.hours.ago).count == 0
      CurrencyWorker.perform_async()
    end
    @currencies = Currency.includes(:countries).order(name: :asc)
    respond_with @currencies
  end

  def show
    @currency = Currency.find(params[:id])
    respond_with @currency
  end

  def update
    @currency = Currency.find(params[:id])
    @currency.update currency_params
    render json: @currency
  end

  private

  def currency_params
    params.require(:currency).permit(:name, country_ids: [])
  end

  def get_currencies
    Report.create(runned_by: "System", info: "Get data from API into an empty database") 
    PerformUpdate.new
  end

end
