class Currency < ActiveRecord::Base
  has_and_belongs_to_many :countries, dependent: :destroy
  validates :code, uniqueness: true
  accepts_nested_attributes_for :countries, allow_destroy: true
end
