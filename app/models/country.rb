class Country < ActiveRecord::Base
  has_and_belongs_to_many :currencies, dependent: :destroy
  validates :code, uniqueness: true
end
