class ApiClient

  attr_reader :parser, :client, :currencies

  def initialize
    @client =  Savon.client wsdl: "http://www.webservicex.net/country.asmx?WSDL"
    @parser = Nori.new
  end

  def currencies
    response = self.client.call(:get_currencies)
    self.parser.parse(response.body[:get_currencies_response][:get_currencies_result])["NewDataSet"]["Table"]  
  end

end
