class PerformUpdate
  def initialize
    ApiClient.new.currencies.each do |c|
      country = Country.create_with(name: c["Name"]).find_or_create_by(code: c["CountryCode"])
      if c["CurrencyCode"]
        currency = Currency.create_with(name: c["Currency"]).find_or_create_by(code: c["CurrencyCode"])
        unless country.currencies.find_by(code: currency.code)
          country.currencies << currency
        end
      end
    end
  end
end
