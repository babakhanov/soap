reportsIndexCtrl = ->
  ($scope, Report, Spinner) ->
    $scope.ready = false
    Spinner.add()
    Report.index "", (response) ->
      $scope.reports = response.reports
      $scope.ready = true
      Spinner.remove()

angular.module "app.reports"
  .controller "reportsIndexCtrl", [
    "$scope"
    "Report"
    "Spinner"
    reportsIndexCtrl()
  ]
