//= require ./core/core
//= require ./shared/shared
//= require ./currencies/currencies
//= require ./reports/reports

angular.module("app", [
  "ngRoute"
  "templates"
  "ngResource"
  "mgcrea.ngStrap.core"
  "mgcrea.ngStrap.modal"
  "mgcrea.ngStrap.helpers.dimensions"
  "app.core"
  "app.shared"
  "app.currencies"
  "app.reports"
])

