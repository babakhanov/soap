currenciesShowCtrl = ->
  ($scope, Currency, $routeParams, $modal, Country, Spinner) ->

    $scope.ready = false
    $scope.search = ""

    $scope.checkedCountries = {}

    getData = ->
      Spinner.add() 
      Currency.show (id: $routeParams.id), (response) -> 
        $scope.currency = response.currency
        for country in $scope.currency.countries
          $scope.checkedCountries[country.id] = true
        Country.index "", (response) ->
          $scope.countries = response.countries
          for country in $scope.countries
            if $scope.checkedCountries[country.id]
              country.checked = true
            else
              country.checked = false
          $scope.ready = true
      Spinner.remove()

    $scope.popup = $modal( 
      scope: $scope
      templateUrl: 'currencies/assign_country_modal.html'
      container: 'body'
      show: false
    )

    $scope.updateCountries = (country) ->
      Spinner.add() 
      country.checked = !country.checked
      checked = {}
      keys = []
      for country in $scope.countries
        checked[country.id] = true if country.checked
      for key of checked
        keys.push key
      Currency.update (id: $routeParams.id, currency: {country_ids: keys}), (response) -> 
        $scope.currency = response.currency
        Spinner.remove() 
        
    getData()

angular.module "app.currencies"
  .controller "currenciesShowCtrl", [
    "$scope"
    "Currency"
    "$routeParams"
    "$modal"
    "Country"
    "Spinner"
    currenciesShowCtrl()
  ]
