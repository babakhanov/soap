currenciesIndexCtrl = ->
  ($scope, Currency, Spinner) ->
    $scope.currencies = null
    Spinner.add()
    Currency.index "", (response) ->
      $scope.currencies = response.currencies
      $scope.ready = true
      Spinner.remove()

angular.module "app.currencies"
  .controller "currenciesIndexCtrl", [
    "$scope"
    "Currency"
    "Spinner"
    currenciesIndexCtrl()
  ]
