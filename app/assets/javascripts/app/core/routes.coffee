angular.module "app.core"
  .config ($routeProvider) ->
    $routeProvider
      .when("/",
        templateUrl: "currencies/index.html",
        controller: "currenciesIndexCtrl"
      ).when('/currencies/:id',
        templateUrl: "currencies/show.html",
        controller: "currenciesShowCtrl"
      ).when('/reports',
        templateUrl: "reports/index.html",
        controller: "reportsIndexCtrl"
      ).otherwise(
        redirectTo: "/"
      )
