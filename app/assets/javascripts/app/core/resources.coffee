angular.module "app.core"
  .factory "Currency", ($resource) ->
    $resource "/currencies/:id", { id: "@id" },
      index:  method: "GET"
      show:   method: "GET"
      update: method: 'PATCH'
  .factory "Country", ($resource) ->
    $resource "/countries", "",
      index:  method: "GET"
  .factory "Report", ($resource) ->
    $resource "/reports", "",
      index:  method: "GET"
