//= require jquery
//= require angular/angular
//= require angular-route/angular-route
//= require angular-rails-templates
//= require angular-resource/angular-resource
//= require angular-strap/dist/modules/compiler
//= require angular-strap/dist/modules/dimensions
//= require angular-strap/dist/modules/modal
//= require_tree ./templates
//= require app/app
