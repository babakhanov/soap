require 'sidekiq/web'

Rails.application.routes.draw do

  root 'pages#index'
  resources :currencies, only: [:index, :show, :update], defaults: { format: :json }
  resources :countries, only: [:index], defaults: { format: :json }
  resources :reports, only: [:index], defaults: { format: :json }

  if Rails.env.development?
    mount Sidekiq::Web => '/sidekiq'
  end

end
